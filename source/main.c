﻿#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <3ds.h>

#include <dlfcn.h>

#define SLDL_IMPLEMENTAION
#include "sldl.h"

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

extern sldl_env env;

static void *_dlopen(const char *module, int flag)
{
  return sldl_dlopen(module);
}

int main(void)
{
  romfsInit();
  sdmcInit();

  dl_setfn(&_dlopen, &sldl_dlerror, &sldl_dlsym, &sldl_dlclose);
  sldl_setenv(&env);  
  
  puts("3DS LuaJIT ljn3ds");
  
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  
  if (luaL_dofile(L, "romfs:/shell.lua")) {
    /* report error */
    FILE *f = fopen("ljn3ds_error.log", "w");
    fputs(luaL_checkstring(L, -1), f);
    fclose(f);
  } 
  
  romfsExit();
  sdmcExit();
  return 0;
}
