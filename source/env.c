#include <stdbool.h>
#include <3ds.h>

#include "sldl.h"

#include "ctru.sym.h"

sldl_env env = {
  { "libctru.so", ctru_symbols, .global = true },
  { NULL, NULL }
};
