/*
  Autogenerated file with gen.lua.
*/

#include "sldl.h"
#include <3ds.h>

static void w_gspWaitForVBlank(void)
{
  gspWaitForVBlank();
}

struct sldl_symbol ctru_symbols[] = {
  { "gfxInitDefault", gfxInitDefault },
  { "gfxFlushBuffers", gfxFlushBuffers },
  { "gfxSwapBuffers", gfxSwapBuffers },
  { "gfxExit", gfxExit },
  { "gspWaitForVBlank", w_gspWaitForVBlank },
  { "consoleInit", consoleInit },
  { "aptMainLoop", aptMainLoop },
  { "hidInit", hidInit },
  { "hidExit", hidExit },
  { "hidScanInput", hidScanInput },
  { "hidKeysDown", hidKeysDown },
  { "swkbdInit", swkbdInit },
  { "swkbdSetStatusData", swkbdSetStatusData },
  { "swkbdInputText", swkbdInputText },
  { (void *)0, (void *)0 }
};
