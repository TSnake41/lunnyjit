typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef int32_t Result;

typedef enum
{
  SWKBD_NONE          = -1,
  SWKBD_INVALID_INPUT = -2,
  SWKBD_OUTOFMEM      = -3,

  SWKBD_D0_CLICK = 0,
  SWKBD_D1_CLICK0,
  SWKBD_D1_CLICK1,
  SWKBD_D2_CLICK0,
  SWKBD_D2_CLICK1,
  SWKBD_D2_CLICK2,

  SWKBD_HOMEPRESSED = 10,
  SWKBD_RESETPRESSED,
  SWKBD_POWERPRESSED,

  SWKBD_PARENTAL_OK = 20,
  SWKBD_PARENTAL_FAIL,

  SWKBD_BANNED_INPUT = 30,
} SwkbdResult;

typedef enum
{
	SWKBD_CALLBACK_OK = 0,
	SWKBD_CALLBACK_CLOSE,
	SWKBD_CALLBACK_CONTINUE,
} SwkbdCallbackResult;

typedef enum
{
	SWKBD_TYPE_NORMAL = 0,
	SWKBD_TYPE_QWERTY,
	SWKBD_TYPE_NUMPAD,
	SWKBD_TYPE_WESTERN,
} SwkbdType;

typedef enum
{
	SWKBD_ANYTHING = 0,
	SWKBD_NOTEMPTY,
	SWKBD_NOTEMPTY_NOTBLANK,
	SWKBD_NOTBLANK_NOTEMPTY = SWKBD_NOTEMPTY_NOTBLANK,
	SWKBD_NOTBLANK,
	SWKBD_FIXEDLEN,
} SwkbdValidInput;

typedef enum
{
	SWKBD_BUTTON_LEFT = 0,
	SWKBD_BUTTON_MIDDLE,
	SWKBD_BUTTON_RIGHT,
	SWKBD_BUTTON_CONFIRM = SWKBD_BUTTON_RIGHT,
	SWKBD_BUTTON_NONE,
} SwkbdButton;

typedef enum
{
	SWKBD_PASSWORD_NONE = 0,
	SWKBD_PASSWORD_HIDE,
	SWKBD_PASSWORD_HIDE_DELAY,
} SwkbdPasswordMode;

enum
{
	SWKBD_FILTER_DIGITS    = 0,
	SWKBD_FILTER_AT        = 1 << 0,
	SWKBD_FILTER_PERCENT   = 1 << 2,
	SWKBD_FILTER_BACKSLASH = 1 << 3,
	SWKBD_FILTER_PROFANITY = 1 << 4,
	SWKBD_FILTER_CALLBACK  = 1 << 5,
};

enum
{
	SWKBD_PARENTAL          = 1 << 0,
	SWKBD_DARKEN_TOP_SCREEN = 1 << 1,
	SWKBD_PREDICTIVE_INPUT  = 1 << 2,
	SWKBD_MULTILINE         = 1 << 3,
	SWKBD_FIXED_WIDTH       = 1 << 4,
	SWKBD_ALLOW_HOME        = 1 << 5,
	SWKBD_ALLOW_RESET       = 1 << 6,
	SWKBD_ALLOW_POWER       = 1 << 7,
	SWKBD_DEFAULT_QWERTY    = 1 << 9,
};

typedef struct { u32 data[0x11]; } SwkbdStatusData;
typedef struct { u32 data[0x291B]; } SwkbdLearningData;
typedef SwkbdCallbackResult (* SwkbdCallbackFn)(void* user, const char** ppMessage, const char* text, size_t textlen);

typedef struct
{
	u16 reading[41];
	u16 word[41];
	u8 language;
	bool all_languages;
} SwkbdDictWord;

typedef struct
{
  const char* initial_text;
  const SwkbdDictWord* dict;
  SwkbdStatusData* status_data;
  SwkbdLearningData* learning_data;
  SwkbdCallbackFn callback;
  void* callback_user;
} SwkbdExtra;

typedef struct
{
  int type;
  int num_buttons_m1;
  int valid_input;
  int password_mode;
  int is_parental_screen;
  int darken_top_screen;
  u32 filter_flags;
  u32 save_state_flags;
  u16 max_text_len;
  u16 dict_word_count;
  u16 max_digits;
  u16 button_text[3][17];
  u16 numpad_keys[2];
  u16 hint_text[65];
  bool predictive_input;
  bool multiline;
  bool fixed_width;
  bool allow_home;
  bool allow_reset;
  bool allow_power;
  bool unknown;
  bool default_qwerty;
  bool button_submits_text[4];
  u16 language;
  int initial_text_offset;
  int dict_offset;
  int initial_status_offset;
  int initial_learning_offset;
  size_t shared_memory_size;
  u32 version;
  SwkbdResult result;
  int status_offset;
  int learning_offset;
  int text_offset;
  u16 text_length;
  int callback_result;
  u16 callback_msg[257];
  bool skip_at_check;
  union
  {
    u8 reserved[171];
    SwkbdExtra extra;
  };
} SwkbdState;

void swkbdInit(SwkbdState* swkbd, SwkbdType type, int numButtons, int maxTextLength);
void swkbdSetStatusData(SwkbdState* swkbd, SwkbdStatusData* data, bool in, bool out);
SwkbdButton swkbdInputText(SwkbdState* swkbd, char* buf, size_t bufsize);

void gfxInitDefault(void);
void gfxExit(void);
void gfxFlushBuffers(void);
void gfxSwapBuffers(void);
void gspWaitForVBlank(void);

typedef enum
{
	GFX_TOP = 0,
	GFX_BOTTOM = 1
} gfxScreen_t;

void *consoleInit(gfxScreen_t screen, void* print_console);

bool aptMainLoop(void);
void hidScanInput(void);
u32 hidKeysDown(void);

Result hidInit(void);
void hidExit(void);

enum
{
	KEY_A       = 1 << 0,
	KEY_B       = 1 << 1,
	KEY_SELECT  = 1 << 2,
	KEY_START   = 1 << 3,
	KEY_DRIGHT  = 1 << 4,
	KEY_DLEFT   = 1 << 5,
	KEY_DUP     = 1 << 6,
	KEY_DDOWN   = 1 << 7,
	KEY_R       = 1 << 8,
	KEY_L       = 1 << 9,
	KEY_X       = 1 << 10,
	KEY_Y       = 1 << 11,
	KEY_ZL      = 1 << 14,
	KEY_ZR      = 1 << 15,
	KEY_TOUCH   = 1 << 20,
	KEY_CSTICK_RIGHT = 1 << 24,
	KEY_CSTICK_LEFT  = 1 << 25,
	KEY_CSTICK_UP    = 1 << 26,
	KEY_CSTICK_DOWN  = 1 << 27,
	KEY_CPAD_RIGHT = 1 << 28,
	KEY_CPAD_LEFT  = 1 << 29,
	KEY_CPAD_UP    = 1 << 30,
	KEY_CPAD_DOWN  = 1 << 31,

	KEY_UP    = KEY_DUP    | KEY_CPAD_UP,
	KEY_DOWN  = KEY_DDOWN  | KEY_CPAD_DOWN,
	KEY_LEFT  = KEY_DLEFT  | KEY_CPAD_LEFT,
	KEY_RIGHT = KEY_DRIGHT | KEY_CPAD_RIGHT,
};