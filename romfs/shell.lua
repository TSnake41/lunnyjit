local bit = require "bit"
local ffi = require "ffi"
local C = ffi.C

-- There is a lot of stuff to define, use a file instead.
do
  local f = io.open("romfs:/ffi.h", "r")
  assert(f, "ffi.h not found in romfs")

  ffi.cdef(f:read "*a")
  f:close()
end

C.gfxInitDefault()
C.hidInit()

-- Initialize console
C.consoleInit(C.GFX_TOP, nil);

local swkbd = ffi.new("SwkbdState[1]")
local swkbd_status = ffi.new("SwkbdStatusData[1]")
local str = ffi.new("char[256]")

ffi.fill(str, 256)

print "3DS LuaJIT shell"

while C.aptMainLoop() do
  C.hidScanInput()

  local kdown = C.hidKeysDown()

  if bit.band(kdown, C.KEY_A) ~= 0 then
    io.write '>'

    C.swkbdInit(swkbd, C.SWKBD_TYPE_NORMAL, 1, -1)
    C.swkbdSetStatusData(swkbd, swkbd_status, false, true);
    C.swkbdInputText(swkbd, str, ffi.sizeof(str));

    local lstr = ffi.string(str)
    io.write(lstr .. '\n')

    local f, err = loadstring(lstr)

    if f then
      local status, err = pcall(f)
      if not status then
        print(err)
      end
    else
      print(err)
    end

    ffi.fill(str, 256)
  end

  if bit.band(kdown, C.KEY_START) ~= 0 then
    break
  end

  C.gfxFlushBuffers()
  C.gfxSwapBuffers()
  C.gspWaitForVBlank()
end

C.gfxExit()
C.hidExit()
